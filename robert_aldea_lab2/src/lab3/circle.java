package lab3;

public class circle {
	private int radius = 3;
	private String color = "red";
	
	public circle(int x)
	{
		radius = x;
	}
	
	public circle(String x)
	{
		color = x;
	}
	
	public int getRadius()
	{
		return radius;
	}
	
	public String getColor()
	{
		return color;
	}
}
