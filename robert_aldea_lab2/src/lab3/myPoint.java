package lab3;

public class myPoint {
	int x,y;
	
	public myPoint()
	{
		x = 0;
		y = 0;
	}
	
	public myPoint(int posX, int posY)
	{
		x = posX;
		y = posY;
	}
	
	public void setX(int posX)
	{
		x = posX;
	}
	
	public int getX()
	{
		return x;
	}
	
	public void setY(int posY)
	{
		y = posY;
	}
	
	public int getY()
	{
		return y;
	}
	
	public void setXY(int posX, int posY)
	{
		x = posX;
		y = posY;
	}
	
	@Override
	public String toString()
	{
		return " x coordinate is "+ x + " and y point coordinate "+ y;
	}
	
	public double distance(int posX, int posY)
	{
		return Math.sqrt((double)((this.getX()-posX)^2 + (this.getY()-posY)^2));
	}
	
	public double distance(myPoint obj)
	{
		return Math.sqrt((double)((this.getX()-obj.getX())^2 + (this.getY()-obj.getY())^2));
	}
}
