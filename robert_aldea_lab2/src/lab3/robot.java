package lab3;

class robot {
	int x;
	
	public robot()
	{
		x = 1;
	}
	
	public void change(int k)
	{
		if(k>=1)
		{
			x+=k;
		}
	}
	
	@Override
	public String toString()
	{
		return "x=" + x;
	}
	
}
