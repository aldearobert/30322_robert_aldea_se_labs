package lab3;

public class ex4 {
	public static void main(String[] args) {
		myPoint a = new myPoint();
		myPoint b = new myPoint(15, 6);
	
		a.setXY(3,4);
		System.out.println("The points are: " + a.toString() + " and " + b.toString()); 
		System.out.println("The distance between them is: " + b.distance(a));
		System.out.println("The distance between the point a random point: " + b.distance(2,1));
	}
}
