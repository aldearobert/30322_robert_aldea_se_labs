package lab5;

public class Controller {
    private static Controller controller;

    private temperatureSensor temperatureSensor = new temperatureSensor();
    private lightSensor lightSensor = new lightSensor();

    private Controller(){}

    public static Controller getController(){
        if(controller == null)
            controller = new Controller();
        return controller;
    }

    public void  control(){
        for(int i = 1; i <= 20; i++) {
            System.out.println("Temperature level is: " + temperatureSensor.readValue());
            System.out.println("Light level is: " + lightSensor.readValue());
            wait(1000);
        }
    }

    public static void wait(int ms){
        try {
            Thread.sleep(ms);
        }
        catch (InterruptedException ex){
            Thread.currentThread().interrupt();
        }
    }
}