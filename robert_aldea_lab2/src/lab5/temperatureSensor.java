package lab5;
import java.util.Random;

public class temperatureSensor extends Sensor{
    @Override
    public int readValue(){
        Random random = new Random();
        return random.nextInt(100);
    }
}