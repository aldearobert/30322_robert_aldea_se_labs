package lab5;

public class ProxyImage implements Image{
 
   private RealImage realImage;
   private RotatedImage rotatedImage;
   private String fileName;
   boolean isRotated;
 
   public ProxyImage(String fileName,boolean rotate){
      this.fileName = fileName;
      isRotated = rotate;
   }
 
   @Override
   public void display() {
      if(isRotated)
      {
          if(rotatedImage == null){
              rotatedImage = new RotatedImage(fileName);
           }
           rotatedImage.display(); 
      }
      else
	   {
    	  if(realImage == null){
         realImage = new RealImage(fileName);
    	  }
	   realImage.display();
	   }
   }
}