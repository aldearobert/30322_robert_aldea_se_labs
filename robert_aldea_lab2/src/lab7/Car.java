package lab7;

public class Car implements java.io.Serializable{
	String Model;
	double Price;
	
	Car(String m, double p)
	{
		Model = m;
		Price = p;
	}
	
	@Override
	public String toString() {
		return Model +" $" + Price;
	}
}
