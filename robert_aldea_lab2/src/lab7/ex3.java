package lab7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ex3 {
	public static void Encrypt(File txt, FileWriter fw) throws IOException{
		char newChar =' ';
		String newString = "";
		int i;
		
	try {
		Scanner scan = new Scanner(txt);
			 while (scan.hasNextLine()) {
			        String txtLine = scan.nextLine();
			        for (i = 0; i < txtLine.length(); i++) {
			        	newChar = (char)((int)txtLine.charAt(i)-1);
			        	newString = newString + newChar;
			        }
			        fw.write(newString);
			 }
		} 
		catch(FileNotFoundException e) {
		System.out.println("An error occurred.");
		e.printStackTrace();
		}
	}
	
	public static void Decrypt(File txt, FileWriter fw) throws IOException{
		char newChar =' ';
		String newString = "";
		int i;
		
	try {
		Scanner scan = new Scanner(txt);
			 while (scan.hasNextLine()) {
			        String txtLine = scan.nextLine();
			        for (i = 0; i < txtLine.length(); i++) {
			        	newChar = (char)((int)txtLine.charAt(i)+1);
			        	newString = newString + newChar;
			        }
			        fw.write(newString);
			 }
		} 
		catch(FileNotFoundException e) {
		System.out.println("An error occurred.");
		e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		File txt = new File("f1");
		File txt2 = new File("f2");
		FileWriter fw = new FileWriter("f2", false);
		FileWriter fw2 = new FileWriter("f3", false);
		Encrypt(txt, fw);
		
        fw.flush();
        fw.close();
        
        Decrypt(txt2, fw2);
        fw2.flush();
        fw2.close();
	}

}
