package lab7;
import java.io.File;  
import java.io.FileNotFoundException;  
import java.util.Scanner;

public class ex2 {
	public static void main(String[] args) {
		int count = 0;
		
		Scanner sc = new Scanner(System.in);
		String in = sc.nextLine();
		char chr = in.charAt(0);
		
		try {	
			File txt = new File("f1");
		
			Scanner scan = new Scanner(txt);
			 while (scan.hasNextLine()) {
			        String txtLine = scan.nextLine();
			        for (int i = 0; i < txtLine.length(); i++) {
					    if (txtLine.charAt(i) == chr) 
					        count++;
					    }
			 }
			 System.out.println(count);
			 scan.close();
			 sc.close();
		}
		catch(FileNotFoundException e) {
		System.out.println("An error occurred.");
		e.printStackTrace();
		}
		
	}
}

