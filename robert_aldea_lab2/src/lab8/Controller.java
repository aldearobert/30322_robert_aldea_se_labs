package lab8;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
public class Controller {
	private static Controller controller;
	
	private FireSensor[] fireSensor;
	private TemperatureSensor tempSensor;
	
	public Controller(){
		 	tempSensor = new TemperatureSensor(15, "system_logs.txt");
	        
	        fireSensor = new FireSensor[4];
	        fireSensor[0] = new FireSensor(false, "system_logs.txt");
	        fireSensor[1] = new FireSensor(false, "system_logs.txt");
	        fireSensor[2] = new FireSensor(false, "system_logs.txt");
	        fireSensor[3] = new FireSensor(false, "system_logs.txt");
	}
	public static Controller getController(){
        if(controller == null){
            controller =  new Controller();
        }
        return controller;
    }
	
	public void updateTemperature(int temp)
	{
		tempSensor.setTemperature(temp);
	}
	
	public void updateFireState(int index, boolean fire)
	{
		fireSensor[index].setFireState(fire);
	}
	
	
}

class FireSensor{
	String file;
	boolean isFire;
	AlarmUnit alarm;
	GsmUnit gsmAlarm;
	FireSensor(boolean fire, String txt)
	{
		file = txt;
		isFire = fire;
		if(isFire == true)
		{
			FireAlarm();
			write("fire and gsm", file);
		}
	}
	public void setFireState(boolean fire)
	{
		isFire = fire;
		if(isFire == true)
		{
				FireAlarm();
				write("fire and gsm", file);
		}
	}
	
	public void FireAlarm()
	{
		alarm.startUnit();
		gsmAlarm.startUnit();
	}
	
	private void write(String txt, String file)
	{
		try{
			PrintWriter fw = new PrintWriter(new FileWriter(file, true));
			fw.write("Started "+txt +" unit.");
			fw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
}

class TemperatureSensor{
	int temperature;
	String file;
	CoolingUnit cool;
	HeatingUnit heat;
	TemperatureSensor(int temp, String txt)
	{
		temperature = temp;
		file = txt;
		if(temperature > 30)
		{
			CoolingStart();
			write("cooling", file);
		}
		else
			if(temperature < 10)
			{
				HeatingStart();
				write("heating", file);
			}
	}
	public void setTemperature(int temp)
	{
		temperature = temp;
		if(temperature > 30)
		{
			CoolingStart();
			write("cooling", file);
		}
		else
			if(temperature < 10)
			{
				HeatingStart();
				write("heating", file);
			}
	}
	
	private void write(String txt, String file)
	{
		try{
			PrintWriter fw = new PrintWriter(new FileWriter(file, true));
			fw.write("Started "+txt +" unit.");
			fw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void CoolingStart()
	{
		cool.startUnit();
	}
	
	public void HeatingStart()
	{
		heat.startUnit();
	}
	
}

interface Unit{
	
	public void startUnit();
}

class AlarmUnit implements Unit{
	public void startUnit() {
		System.out.println("Alarm started");
	}
}

class GsmUnit implements Unit{
	public void startUnit() {
		System.out.println("Owner Called");
	}
}

class HeatingUnit implements Unit{
	public void startUnit() {
		System.out.println("Heating started");
	}
}

class CoolingUnit implements Unit{
	public void startUnit() {
		System.out.println("Cooling started");
	}
}