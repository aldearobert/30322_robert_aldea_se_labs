package robert_aldea_lab2;

import java.util.*;  

public class ex4 {
	public static void main(String[] args)
	{
	Scanner scan = new Scanner(System.in);
	int max = 0;
	
	int n = scan.nextInt();
	Vector<Integer> v = new Vector<>(n);
	
	for (int i = 0; i <= n; i++)
	{
		 v.add(scan.nextInt());
		
		if (v.get(i)>max)
			max = v.get(i);
	}
	System.out.println("The maximum element of the vector is " + max);
}
}
