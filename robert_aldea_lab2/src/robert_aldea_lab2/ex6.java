package robert_aldea_lab2;

import java.util.Scanner;

public class ex6 {
	
	public static long factorial(int n) {
		if(n>1)
		{
			return n * factorial(n-1);
		}
		else
			return 1;
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int f = 1;

		int n = scan.nextInt();
		
		for(int i = 0; i < n; i++)
		{
			f *=i+1;
		}
		
		System.out.println("Non recursive factorial:" + f);
		f = 1;
		
		System.out.print("Recursive factorial:" + factorial(n));
	}

}
