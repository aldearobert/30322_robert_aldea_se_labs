package robert_aldea_lab2;

import java.util.Scanner;

public class ex7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		boolean flag = false;
		Scanner scan = new Scanner(System.in);
		int rand = (int)((Math.random() * (10 - 1)) + 1);
		
		for (int i = 0; i < 3; i++)
		{
			System.out.println("Guess the number!");
			int guess = scan.nextInt();
			if (guess == rand)
			{
				flag = true;
				break;
			}
			else if(guess>rand)
				System.out.println("Too high!");
			else
				System.out.println("Too low!");
		}
		if(flag)
			System.out.println("You found the number!");
		else
			System.out.println("You lost!");
		

	}

}