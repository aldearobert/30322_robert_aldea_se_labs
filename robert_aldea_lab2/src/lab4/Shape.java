package lab4;

abstract class Shape {
	protected String color;
	protected boolean isFilled;
	public Shape() {
		
	}
	public Shape(String col,boolean filled) {
		color = col;
		isFilled = filled;
	}
	public String getColor()
	{
		return color;
	}
	public void setColor(String col)
	{
		color = col;
	}
	public boolean getFilled()
	{
		return isFilled;
	}
	public void setFilled(boolean fill)
	{
		isFilled = fill;
	}
	public String toString()
	{
			return "A shape of color " + color + " that is filled[T/F] "+isFilled;
	}

}
