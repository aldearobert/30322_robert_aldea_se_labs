package lab4;

public class Book {
	String name;
	Author[] author;
	double price;
	int qtyInStock;
	
	Book(String n, Author[] aut, double p)
	{
		name = n;
		author = aut;
		price = p;
	}
	Book(String n, Author[] aut, double p, int q)
	{
		name = n;
		author = aut;
		price = p;
		qtyInStock = q;
	}
	public String getName()
	{
		return name;
	}
	public Author[] getAuthor()
	{
		return author;
	}
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double p)
	{
		price = p;
	}
	public int getQuantity()
	{
		return qtyInStock;
	}
	public void setQuantity(int q)
	{
		qtyInStock = q;
	}
	
	public void printAuthors()
	{
		for(int i = 0; i < author.length; i++)
			System.out.println(author[i].getName());
	}
}
