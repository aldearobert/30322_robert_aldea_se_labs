package lab4;

public class ex4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Author[] aut = {new Author("John Smith", "johnsmith@gmail.com",'M'), new Author("Steve Johns", "stevej@gmail.com",'M')};
		Author[] aut2 = {new Author("John Not Smith", "johnsmith@gmail.com",'M')};
		
		Book b1 = new Book("Math", aut, 5.0, 35);
		b1.printAuthors();
		System.out.println(b1.getName());
		System.out.println(b1.getQuantity());
		
		Book b2 = new Book("Math but more math", aut2, 6.0);
		System.out.println(b2.getName());
		b2.printAuthors();
		b2.setQuantity(33);
		System.out.println(b2.getQuantity());
	}

}
