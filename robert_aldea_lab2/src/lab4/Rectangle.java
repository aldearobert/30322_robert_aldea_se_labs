package lab4;

public class Rectangle extends Shape{
	protected double width;
	protected double length;
	public Rectangle() {
		// TODO Auto-generated constructor stub
	}
	public Rectangle(double w, double l){
		width = w;
		length = l;
	}
	public Rectangle(double w, double l, String col, boolean fill){
		width = w;
		length = l;
		super.color = col;
		super.isFilled = fill;
	}
	
	public double getWidth()
	{
		return width;
	}
	public void setWidth(double x)
	{
		width = x;
	}
	
	public double getLength()
	{
		return length;
	}
	public void setLength(double x)
	{
		length = x;
	}
	public double getArea()
	{
		return length*width;
	}
	
	public double getPerimeter()
	{
		return 2*length + 2*width;
	}
	public String toString()
	{
		return "A rectangle with width "+ width+ " and length " + length + " that is a subclass of "+ super.toString();
	}
}
