package lab4;

public class Circle extends Shape{
	protected double radius;
	
	public Circle()
	{
		super(); //always call super in all constructors to initialized the inherited attributes
		radius = 1;
	} 
	
	public Circle(double x)
	{
		radius = x;
	}
	
	public Circle(double x, String col, boolean fill)
	{
		radius = x;
		super.color = col;
		super.isFilled = fill;
	}
	
	public double getRadius()
	{
		return radius;
	}
	public void setRadius(double x)
	{
		radius = x;
	}
	
	public double getArea()
	{
		return Math.PI*radius*radius;
	}
	
	public double getPerimeter()
	{
		return Math.PI * 2 * radius;
	}
	
	public String toString()
	{
		return "A circle of radius " + radius + " that is a subclass of " + super.toString();
	}
	
}
