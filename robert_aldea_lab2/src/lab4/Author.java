package lab4;

public class Author{
	private String name;
	private String email;
	private char  gender;
	
public Author(String n, String e, char g)
{
	name = n;
	email = e;
	if(gender == 'M' || gender == 'm' || gender == 'F' || gender == 'f')
		gender = g;
	else
		gender = 'F';
}

public String getName()
{
	return name;
}

public void setEmail(String e)
{
	email = e;
}

public String getEmail()
{
	return email;
}

public char getGender()
{
	return gender;
}

@Override
public String toString()
{
	return name + "(" + gender + ") at " + email;
}
}