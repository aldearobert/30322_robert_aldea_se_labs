package lab4;

public class Square extends Rectangle{
	double side;
	public Square() {
		// TODO Auto-generated constructor stub
	}
	public Square(double s){
		side = s;
		super.width = s;
		super.length = s;
	}
	public Square(double s, String col, boolean fill){
		side = s;
		super.width = s;
		super.length = s;
		super.color = col;
		super.isFilled = fill;
	}
	public void setWidth(double x)
	{
		super.width = x;
		super.length = x;
		side = x;
	}
	public void setLength(double x)
	{
		super.length = x;
		super.width = x;
		side = x;
	}
	public String toString()
	{
		return "A square with sides "+ side + " that is a subclass of "+ super.toString();
	}
}
