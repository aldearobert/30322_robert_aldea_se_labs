package lab4;

public class ex5 {
	public static void main(String[] args) {
		Circle cir = new Circle(5);
		Cylinder cyl = new Cylinder(10,7);
		
		System.out.println(cir.getRadius() +" "+ cir.getArea());
		System.out.println(cyl.getRadius()+" "+ cyl.getArea());
		System.out.println(cyl.getVolume());
	}
}
