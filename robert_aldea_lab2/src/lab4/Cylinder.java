package lab4;

public class Cylinder extends Circle{
	double height;
	public Cylinder() {
		
	}
	public Cylinder(double x) {
		height = x;
	}
	public Cylinder(double x, double r) {
		height = x;
		super.radius = r;
	}
	public double getHeight()
	{
		return height;
	}
	public double getVolume()
	{
		return Math.PI*radius*2*height;
	}
	
	@Override
	public double getArea()
	{
		return Math.PI * radius * (radius + height);
	}
}
