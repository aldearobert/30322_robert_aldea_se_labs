package lab9;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.util.*;

public class ex2 extends JFrame{
	 JLabel count;
     JButton bIncrease;
     static int cValue = 0;
	
     ex2(){
           setTitle("Increment Button");
           setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
           init();
           setSize(200,80);
           setVisible(true);
     }
     public void increaseCValue()
     {
    	 cValue+=1;
    	 count.setText("" + cValue);
     }
     public void init(){
    	 
         this.setLayout(null);
         int width=100;int height = 20;

         count = new JLabel(""+cValue);
         count.setBounds(10, 10, width, height);

         bIncrease = new JButton("Increase");
         bIncrease.setBounds(80,10,width, height);
         bIncrease.addActionListener(new bIncreaseAction());
         
         add(count); add(bIncrease);

   }

     public static void main(String[] args) {
           ex2 a = new ex2();
     }
     
     class bIncreaseAction implements ActionListener{
    	 
         public void actionPerformed(ActionEvent e) {
               ex2.this.increaseCValue();
         }    
   }
}
