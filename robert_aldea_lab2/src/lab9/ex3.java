package lab9;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.swing.*;
import java.util.*;
 
public class ex3 extends JFrame{
      JLabel text;
      JTextField tText;
      JTextArea tArea;
      JButton bConfirm;
 
      ex3(){
            setTitle("Open file");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(200,400);
            setVisible(true);
      }
 
      public void init(){
 
            this.setLayout(null);
            int width=80;int height = 20;
 
            text = new JLabel("Enter file:");
            text.setBounds(10, 50, width, height);
 
            tText = new JTextField();
            tText.setBounds(70,50,width, height);
 
            bConfirm = new JButton("Send");
            bConfirm.setBounds(10,150,width, height);
 
            bConfirm.addActionListener(new TratareButonLoghin());
 
            tArea = new JTextArea();
            tArea.setBounds(10,180,150,200);
 
            add(text);add(tText);add(bConfirm);
            add(tArea);
 
      }
 
      public static void main(String[] args) {
            new ex3();
      }
 
      class TratareButonLoghin implements ActionListener{
 
            public void actionPerformed(ActionEvent e) {
            	try {
					FileInputStream fileIn = new FileInputStream(ex3.this.tText.getText());
					ex3.this.tArea.setText("");
					Scanner scan = new Scanner(fileIn);
					 while (scan.hasNextLine()) {
					        String txtLine = scan.nextLine();
					        ex3.this.tArea.append(txtLine+"\n");
					 }
					 scan.close();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }    
      }
}
