package lab9;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.util.*;

public class ex4 extends JFrame{
     JButton[] bXO = new JButton[9];
     JLabel won;
     public char XO = 'X';
	
     ex4(){
           setTitle("XO");
           setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
           init();
           setSize(300,300);
           setVisible(true);
     }
     
     public void changeXO()
     {
    	 this.checkWinState();
    	 if(XO == 'X')
    	 {
    		 XO = 'O';
    	 }
    	 else XO = 'X';
     }
     
     public void winScreen(String p) {
    	 bXO[0].setVisible(false);
    	 bXO[1].setVisible(false);
    	 bXO[2].setVisible(false);
    	 bXO[3].setVisible(false);
    	 bXO[4].setVisible(false);
    	 bXO[5].setVisible(false);
    	 bXO[6].setVisible(false);
    	 bXO[7].setVisible(false);
    	 bXO[8].setVisible(false);
    	 
    	 won.setText(p);
     }
     
     public void checkWinState()
     {
    	 if((bXO[0].getText().equals(""+XO) && bXO[1].getText().equals(""+XO) && bXO[2].getText().equals(""+XO)) ||
            (bXO[3].getText().equals(""+XO) && bXO[4].getText().equals(""+XO) && bXO[5].getText().equals(""+XO)) ||
            (bXO[6].getText().equals(""+XO) && bXO[7].getText().equals(""+XO) && bXO[8].getText().equals(""+XO)) ||
            
            (bXO[0].getText().equals(""+XO) && bXO[3].getText().equals(""+XO) && bXO[6].getText().equals(""+XO)) ||
            (bXO[1].getText().equals(""+XO) && bXO[4].getText().equals(""+XO) && bXO[7].getText().equals(""+XO)) ||
            (bXO[2].getText().equals(""+XO) && bXO[5].getText().equals(""+XO) && bXO[8].getText().equals(""+XO)) ||
            
            (bXO[0].getText().equals(""+XO) && bXO[4].getText().equals(""+XO) && bXO[8].getText().equals(""+XO)) ||
            (bXO[6].getText().equals(""+XO) && bXO[4].getText().equals(""+XO) && bXO[2].getText().equals(""+XO)))
    	 {
    		 if(XO == 'X')
    		 	winScreen("Player 1 won");
    		 else
    			 winScreen("Player 2 won");
    	 }	
     }
     
     public void init(){
    	 
         this.setLayout(null);
         int width=50;int height = 50;

         bXO[0] = new JButton(" ");
         bXO[0].setBounds(50,50,width, height);
         bXO[0].addActionListener(new bIncreaseAction());
         
         bXO[1] = new JButton(" ");
         bXO[1].setBounds(100,50,width, height);
         bXO[1].addActionListener(new bIncreaseAction());
         
         bXO[2] = new JButton(" ");
         bXO[2].setBounds(150,50,width, height);
         bXO[2].addActionListener(new bIncreaseAction());
         
         bXO[3] = new JButton(" ");
         bXO[3].setBounds(50,100,width, height);
         bXO[3].addActionListener(new bIncreaseAction());
         
         bXO[4] = new JButton(" ");
         bXO[4].setBounds(100,100,width, height);
         bXO[4].addActionListener(new bIncreaseAction());
         
         bXO[5] = new JButton(" ");
         bXO[5].setBounds(150,100,width, height);
         bXO[5].addActionListener(new bIncreaseAction());
         
         bXO[6] = new JButton(" ");
         bXO[6].setBounds(50,150,width, height);
         bXO[6].addActionListener(new bIncreaseAction());
         
         bXO[7] = new JButton(" ");
         bXO[7].setBounds(100,150,width, height);
         bXO[7].addActionListener(new bIncreaseAction());
         
         bXO[8] = new JButton(" ");
         bXO[8].setBounds(150,150,width, height);
         bXO[8].addActionListener(new bIncreaseAction());
         
         won = new JLabel("");
         won.setBounds(100, 100, 100, 100);
         
         add(bXO[0]);add(bXO[1]);add(bXO[2]);add(bXO[3]);add(bXO[4]);add(bXO[5]);add(bXO[6]);add(bXO[7]);add(bXO[8]);
         add(won);
   }

     public static void main(String[] args) {
           ex4 a = new ex4();
     }
     
     class bIncreaseAction implements ActionListener{
    	 
         public void actionPerformed(ActionEvent e) {
        	 JButton btn = (JButton)e.getSource();
        	 if(btn.getText()==" ") {
        	 	btn.setText("" + ex4.this.XO);
        	 	ex4.this.changeXO();
        	 }
         }    
   }}
