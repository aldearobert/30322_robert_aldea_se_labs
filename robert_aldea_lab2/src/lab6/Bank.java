package lab6;
//acc should be a list not an array
public class Bank {
	private BankAccount[] acc;
	private static int i = 0;
	
	public void addAccount(long bal, String own)
	{
		this.acc[i] = new BankAccount(own, bal);
		i+=1;
	}
	
	public void printAccounts()
	{
		for(int j = 0; j<=i; j++)
		{
			System.out.println(j+ ". " + acc[j].toString());
		}
	}
	
	public void printAccounts(long minBal, long maxBal)
	{
		for(int j = 0; j<=i; j++)
		{
			if(acc[j].getBalance()>=minBal && acc[j].getBalance()<=maxBal)
				System.out.println(j+ ". " + acc[j].toString());
		}
	}
	
	public BankAccount getAccount(String own)
	{	
		for(int j = 0; j<=i; j++)
		{
			if(acc[j].getOwner().equals(own))
			{
				return acc[j];
			}
		}
		return null;
	}
}
