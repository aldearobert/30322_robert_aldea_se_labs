package lab6;

public class Definition {
	 private String description;

	public Definition(String desc){
		this.description = desc;
	}

	public void setDescription(String desc) {
		this.description = desc;
	}
	 
	public String getDescription() {
	    return description;
	}


	@Override
	public String toString(){
	    return description;
	}
}
	

