package lab6;

public class BankAccount {
	private String Owner;
	private long balance;
	
	BankAccount(String name, long amount){
		this.Owner = name;
		this.balance = amount;
	}	
	
	public void withdraw(long amount)
	{
		balance += amount;
	}
	
	public void deposit(long amount)
	{
		balance -= amount;
	}
	
	public int hashCode(){
		return (int)balance;
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof BankAccount){
			BankAccount p = (BankAccount)obj;
			return balance == p.balance;

		}
		return false;
	}
	
	public long getBalance()
	{
		return balance;
	}
	
	public String getOwner()
	{
		return Owner;
	}
	
	@Override
	public String toString()
	{
		return Owner + " " + balance;
	}
	
	public static void main(String[] args) {

		BankAccount p1 = new BankAccount("Alin",12345);
		BankAccount p2 = new BankAccount("Calin",12345);

		if(p1.equals(p2))
				System.out.println(p1+" and "+p2+ " are equals");
			else
				System.out.println(p1+" and "+p2+ " are NOT equals");

		if(p1.Owner.equals(p2.Owner))
			System.out.println(p1+" and "+p2+" have the same names");
		else
			System.out.println(p1+" and "+p2+" have different names");

}
}
