package lab6;

import java.util.TreeSet;

public class Bank2 {
	private TreeSet<BankAccount> acc = new TreeSet<>();
	
	public void addAccount(long bal, String own)
	{
		acc.add(new BankAccount(own, bal));
	}
	
	public void printAccounts()
	{
		for(BankAccount a : acc)
		{
			 System.out.println(a.toString());
		}
	}
	
	public void printAccounts(long minBal, long maxBal)
	{
		for(BankAccount a : acc)
		{
			if(a.getBalance()>=minBal && a.getBalance()<=maxBal)
			 	System.out.println(a.toString());
		}
	}
	
	public BankAccount getAccount(String own)
	{	
		for(BankAccount a : acc)
		{
			if(a.getOwner().equals(own))
			{
				return a;
			}
		}
		return null;
	}
}