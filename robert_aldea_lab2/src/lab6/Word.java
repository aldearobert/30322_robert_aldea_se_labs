package lab6;
//the equals and hashCode must be implemented
public class Word {
	 private String word;

	public Word(String desc){
		this.word = desc;
	}

	public void setDescription(String desc) {
		this.word = desc;
	}
	 
	public String getDescription() {
	    return word;
	}

	@Override
	public String toString(){
	    return word;
	}
}

