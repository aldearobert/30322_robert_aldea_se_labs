package lab6;
import java.util.*;

public class Dictionary {
	 HashMap<Word, Definition> dictionary = new HashMap<Word, Definition>();
	 
	 public void addWord(Word word, Definition def){
	        dictionary.put(word, def);
	 }

	 public Definition getDefinition(String word){

	        for(Word w : dictionary.keySet()){
	            if(w.toString().equals(word))
	                return dictionary.get(w);
	        }

	        return null;
	 }

	 public Set<Word> getAllWords(){
	        Set<Word> words;
	        
	        words = dictionary.keySet();
	        return words;
	 }


	 public List<String> getAllDefinitions(){
	        List<String> definitions = new ArrayList<String>();

	        for(Word w : dictionary.keySet()){
	            definitions.add(dictionary.get(w).toString());
	        }
	        return definitions;
	 }

	 public void listDictionary(){
	        for(Word w : dictionary.keySet()){
	            String word = w.toString();
	            String def = dictionary.get(w).toString();
	            System.out.println(word + "=" + def);
	        }
	 }
}
