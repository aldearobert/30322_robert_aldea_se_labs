package lab6;
import java.util.*;

public class ConsoleMenu {
	 public static void main(String[] args){
		 	Dictionary dict = new Dictionary();
		 
		 	Scanner scan = new Scanner(System.in);
		 	
		 	int opt;
	        String word, def;
	        
	        do {
	        System.out.println("Choose an option: ");
            System.out.println("1 - Add new word and its definition");
            System.out.println("2 - Search an existing words definition");
            System.out.println("3 - List all words in the dictionary");
            System.out.println("4 - List all definitions in the dictionary");
            System.out.println("5 - List everything currently in the dictionary");
            System.out.println("0 - Exit the dictionary1");

            opt = scan.nextInt();
            scan.nextLine();

            switch (opt){
                case 1:
                    System.out.println("word= ");
                    word = scan.nextLine();
                    
                    System.out.println("definition= ");
                    def = scan.nextLine();
                    dict.addWord(new Word(word), new Definition(def));
                    break;

                case 2:
                    System.out.println("word= ");
                    word = scan.nextLine();

                    if(dict.getDefinition(word) != null){
                        System.out.println(word + "= " + dict.getDefinition(word));
                    }
                    else System.out.println(word + " is not in the dictionary");
                    break;

                case 3:
                    Set<Word> words = dict.getAllWords();
                    for(Word w : words){
                        System.out.println(w);
                    }
                    break;

                case 4:
                    List<String> definitions = dict.getAllDefinitions();

                    for(String d : definitions){
                        System.out.println(d);
                    }
                    break;

                case 5:
                    dict.listDictionary();
                    break;
                case 0:
                	break;
                default:
                	System.out.println("Invalid choice");
                	break;
            }
        }while(opt != 0);
	 }
}