package lab7coffee_example;

public class CoffeeNoException extends Exception{
    int c;
    public CoffeeNoException(int c,String msg) {
          super(msg);
          this.c = c;
    }

    int getCoffeeNo(){
          return c;
    }
}
