package lab7coffee_example;

public class CoffeeDrinker {
	 void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException, CoffeeNoException{
		 if(c.getCoffeeNo()>10)
             throw new CoffeeNoException(c.getCoffeeNo(),"No more coffee!");
		 
		 if(c.getTemp()>60)
               throw new TemperatureException(c.getTemp(),"Coffee is to hot!");
         if(c.getConc()>50)
               throw new ConcentrationException(c.getConc(),"Coffee concentration to high!");
         System.out.println("Drink coffee:"+c);
   }
}
