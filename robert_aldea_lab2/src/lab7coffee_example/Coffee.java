package lab7coffee_example;

public class Coffee {
	private int temp;
    private int conc;
    private static int coffeeNo = 0;

    Coffee(int t,int c){
    	temp = t;
    	conc = c;
        coffeeNo +=1;
    	}
    int getTemp(){
    	return temp;
    	}
    int getConc(){
    	return conc;
    	}
    
    public int getCoffeeNo(){
  	    return coffeeNo;
    	}
    public String toString(){
    	return "[cofee temperature="+temp+":concentration="+conc+"]";
    	}
}//.class

