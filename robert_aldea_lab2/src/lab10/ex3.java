package lab10;


public class ex3 extends Thread{
	static int c = 0;
    ex3(String name){
        super(name);
  }

  public void run(){
        for(int i=0;i<100;i++){
              System.out.println(getName() + " count = "+c);
              c+=1;
              try {
                    Thread.sleep(100);
              } catch (InterruptedException e) {
                    e.printStackTrace();
              }
        }
        System.out.println(getName() + " job finalised.");
  }

  public static void main(String[] args) {
	    ex3 c1 = new ex3("counter1");
	    ex3 c2 = new ex3("counter2");

        c1.run();
        c2.run();
  }
}
