package lab10;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.util.*;

public class ex6 extends Thread{
	static long c = 0;
	boolean paused = true;
    ex6(String name){
        super(name);
  }

  public void run(){
	  	UI a = new UI();
        while(true) {
              if(!paused)
            	  c+=1;
              a.readValue(c);
              try {
                    Thread.sleep(1000);
              } catch (InterruptedException e) {
                    e.printStackTrace();
              }
        }
  }
  
  public long getC() {
	  return c;
  }
  
  public void changePause() {
	  paused = !paused;
  }
  
  public void resetValue() {
	  c = 0;
	  if(!paused)
		  paused = true;
  }
  
  
  public class UI extends JFrame{
		 JLabel count;
	     JButton bPause;
	     JButton bReset;
		
	     UI(){
	           setTitle("Increment Button");
	           setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	           init();
	           setSize(200,100);
	           setVisible(true);
	     }
	     public void readValue(long c)
	     {
	    	 count.setText("" + c);
	     }
	     
	     public void setPause()
	     {
	    	 changePause();
	     }
	     
	     public void setReset()
	     {
	    	 resetValue();
	     }
	     
	     public void init(){
	    	 
	         this.setLayout(null);
	         int width=100;int height = 20;

	         count = new JLabel(""+0);
	         count.setBounds(10, 10, width, height);

	         bPause = new JButton("Start/Pause");
	         bPause.setBounds(80,10,width, height);
	         bPause.addActionListener(new bPauseAction());
	         
	         bReset = new JButton("Reset");
	         bReset.setBounds(80,30,width, height);
	         bReset.addActionListener(new bResetAction());
	         
	         add(count); add(bPause); add(bReset);

	   }
	     class bPauseAction implements ActionListener{
	     	 
	         public void actionPerformed(ActionEvent e) {
	               UI.this.setPause();
	         }    
	     }
	     
	     class bResetAction implements ActionListener{
	     	 
	         public void actionPerformed(ActionEvent e) {
	               UI.this.setReset();
	         }    
	     }
  }
  public static void main(String[] args) {
	    ex6 c1 = new ex6("counter1");
	    
        c1.run();
        }
}

